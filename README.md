Schematic Chest
---------------

This mod allows the placement of a temporary chest item which is then detected and filled
with items depending on which nodes are detected nearby.

schem_chest:add_chest(definition)

  Definition includes:
    'node_nearby'      Node detected nearby to allow chest to be filled.
    'search_radius'    Search radius to look for above node.
    'chest_node'       Node placed and filled, usually "default:chest"
    'chest_contents'   Table containing contents and values for appearing.
          'name'       Name of item e.g. "default:diamond".
          'max'        Total items in stack which is randomly chosen.
          'chance'     Chance of item appearing which is randomly chosen.
          'min_wear'   Minimum wear for tools (1 to 65535).
          'max_wear'   Maximum wear for tools (randomly chosen between both).

```
schem_chest:add_chest({
	node_nearby = "default:stone",
	search_radius = 3,
	chest_node = "default:chest",
	chest_contents = {
		{name = "default:gold_ingot", max = 2, chance = 1},
		{name = "default:diamond", max = 2, chance = 3},
		{name = "default:mese", max = 1, chance = 5},
		{name = "default:pick_stone", max = 1, min_wear = 500, max_wear = 65535, chance = 1}
	}
})
```

Note: A node_nearby entry for "air" could be added as a last resort incase previous entries were never detected.
