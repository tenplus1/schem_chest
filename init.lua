schem_chest = {}

local list = {}

function schem_chest:add_chest(def)
	table.insert(list, def)
end


-- temporary schematic chest
minetest.register_node("schem_chest:chest", {
	description = "Schematic Chest",
	tiles = {
		"default_chest_top.png", "default_chest_top.png",
		"default_chest_side.png", "default_chest_side.png",
		"default_chest_side.png", "default_chest_front.png"
	},
	paramtype2 = "facedir",
	groups = {dig_immediate = 2, unbreakable = 1},
	is_ground_content = false,
	on_blast = function() end
})


-- function to fill chest at position
local function fill_chest(pos, items)

	local stacks = items or {}
	local meta = minetest.get_meta(pos)
	local inv = meta and meta:get_inventory()
	local size = inv and inv:get_size("main")
	local stack

	if not inv then return end

	-- loop through inventory
	for _, def in ipairs(stacks) do

		if math.random((def.chance or 1)) == 1 then

			-- only add if item existd
			if minetest.registered_items[def.name] then

				stack = ItemStack(def.name .. " " .. math.random((def.max or 1)))

				-- if wear levels found choose random wear between both values
				if def.max_wear and def.min_wear then

					stack:set_wear(65535 - (math.random(def.max_wear, def.min_wear)))
				end

				-- set stack in random position
				inv:set_stack("main", math.random(size), stack)
			end
		end
	end
end


-- Abm to look for temporary schematic chest and fill
minetest.register_abm({
	label = "Schematic Chest",
	nodenames = {"schem_chest:chest"},
	interval = 5,
	chance = 1,
	catch_up = false,

	action = function(pos, node)

		for _, def in ipairs(list) do

			-- if node nearby found then place container and fill
			if minetest.find_node_near(pos, def.search_radius, {def.node_nearby}) then

				minetest.set_node(pos, {name = def.chest_node, param2 = node.param2})

				fill_chest(pos, def.chest_contents)
			end
		end
	end
})


print("[MOD] Schematic Chest")


--[[ Test

schem_chest:add_chest({
	node_nearby = "default:stone",
	search_radius = 3,
	chest_node = "default:chest",
	chest_contents = {
		{name = "default:gold_ingot", max = 2, chance = 1},
		{name = "default:diamond", max = 2, chance = 3},
		{name = "default:mese", max = 1, chance = 5},
		{name = "default:pick_stone", min_wear = 500, max_wear = 65535, chance = 1}
	}
})
]]
